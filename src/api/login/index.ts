import request from '@/utils/request';
import { AxiosPromise } from 'axios';


/**
 * 登录表单类型声明
 */
 export interface LoginFormData {
  account: string;
  pwd: string;
}

/**
 * 登录响应类型声明
 */
export interface LoginResponseData {
  token: string;
  name: string;
  companyId: string;
}
/**
 * 登录
 * @param data
 */
export function login(data: LoginFormData): AxiosPromise<LoginResponseData> {
  return request({
    url: '/tenant/user/login',
    method: 'post',
    data: data,
    headers: {
      Authorization: 'Basic bWFsbC1hZG1pbi13ZWI6MTIzNDU2', // 客户端信息Base64明文：mall-admin-web:123456
    },
  });
}

/**
 * 注销
 */
export function logout() {
  return request({
    url: '/youlai-auth/oauth/logout',
    method: 'delete',
  });
}


/**
 * 验证码类型声明

 export interface Captcha {
  img: string;
  uuid: string;
}
 */
/**
 * 获取图片验证码

export function getCaptcha(): AxiosPromise<Captcha> {
  return request({
    url: '/captcha?t=' + new Date().getTime().toString(),
    method: 'get',
  });
}
 */