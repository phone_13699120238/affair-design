import request from '@/utils/request';
import { AxiosPromise } from 'axios';

export interface AppMenuTreeNode {
    id: string;
    parentId: string;
    name: string;
    nodeType: number;
    list: AppMenuTreeNode[];
  }


export function appMenuTree(appId: string): AxiosPromise<AppMenuTreeNode[]> {
    return request({
      url: '/affair/appMenu/tree/'+appId,
      method: 'get'
    });
}


export function addAppMenuNode(appId: string,data:AppMenuTreeNode) {
    return request({
      url: '/affair/appMenu/addNode/'+appId,
      data:data,
      method: 'post'
    });
}

export function editAppMenuNode(appId: string,data:AppMenuTreeNode) {
    return request({
      url: '/affair/appMenu/editNode/'+appId,
      data:data,
      method: 'post'
    });
}


export function removeAppMenuNode(appId: string,data:String[]) {
    return request({
      url: '/affair/appMenu/removeNode/'+appId,
      data:data,
      method: 'post'
    });
}