import request from '@/utils/request';
import { AxiosPromise } from 'axios';


export interface FormAttrSearch {
    formId: string;
    pageNum: number;
    pageSize: number;
}

export interface FormAttr {
    id?: string;
    formId: string;
    name: string;
    pyName?:string;
    type: number;
    valueCountType: number;
    showAttr?: string;
    formAttrSelect?: {
        formId?: string;
        selectList?: [
         {
            key: number;
            name: string;
         }
        ]
    }
}

export interface FormAttrPage {
  pageNum: number;
  pageSize: number;
  total: number;
  list: FormAttr[]
}

export function formAttrSearch(data: FormAttrSearch): AxiosPromise<FormAttrPage> {
  return request({
    url: '/affair/formAttr/search',
    data: data,
    method: 'post'
  });
}

export function formAttrList(data: FormAttrSearch): AxiosPromise<FormAttr[]> {
    return request({
      url: '/affair/formAttr/list',
      data: data,
      method: 'post'
    });
}


export function addFormAttr(data:FormAttr) {
    return request({
      url: '/affair/formAttr/add',
      data:data,
      method: 'post'
    });
}

export function editFormAttr(data:FormAttr) {
    return request({
      url: '/affair/formAttr/edit',
      data:data,
      method: 'post'
    });
}


export function removeFormAttr(data:String[]) {
    return request({
      url: '/affair/formAttr/remove',
      data: data,
      method: 'post'
    });
}