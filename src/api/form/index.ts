import request from '@/utils/request';
import { AxiosPromise } from 'axios';

export interface FormTreeNode {
    id: string;
    parentId: string;
    name: string;
    nodeType: number;
    list: FormTreeNode[];
  }

  /**
 * 获取目录树
 * @param data
 */
export function formTree(): AxiosPromise<FormTreeNode[]> {
    return request({
      url: '/affair/form/tree',
      method: 'get'
    });
}


export function addFormNode(data:FormTreeNode) {
    return request({
      url: '/affair/form/addNode',
      data:data,
      method: 'post'
    });
}

export function editFormNode(data:FormTreeNode) {
    return request({
      url: '/affair/form/editNode',
      data:data,
      method: 'post'
    });
}


export function removeFormNode(data:String[]) {
    return request({
      url: '/affair/form/removeNode',
      data:data,
      method: 'post'
    });
}