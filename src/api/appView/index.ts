import request from '@/utils/request';
import { AxiosPromise } from 'axios';



export interface AppView {
    id?: string;
    formId: string;
    appMenuId: string;
    model?:{
      cells?: any[]
    }
}

export function saveAppView(data:AppView) {
    return request({
      url: '/affair/appView/save',
      data:data,
      method: 'post'
    });
}

export function saveModelAppView(data:AppView) {
    return request({
      url: '/affair/appView/saveModel',
      data:data,
      method: 'post'
    });
}


export function getAppView(appMenuId:String):AxiosPromise<AppView> {
    return request({
      url: '/affair/appView/get/'+appMenuId,
      method: 'get'
    });
}

export function getModelAppView(appMenuId:String):AxiosPromise<AppView> {
  return request({
    url: '/affair/appView/getModel/'+appMenuId,
    method: 'get'
  });
}