import request from '@/utils/request';
import { AxiosPromise } from 'axios';



export interface ViewPage {
    id?: string;
    appViewId: string;
    name: string;
    design?: any
}

export function viewPageListByAppMenuId(appMenuId: string): AxiosPromise<ViewPage[]> {
  return request({
    url: '/affair/viewPage/listByAppMenuId/'+appMenuId,
    method: 'get'
  });
}


export function saveDesignViewPage(data:ViewPage) {
    return request({
      url: '/affair/appView/save',
      data:data,
      method: 'post'
    });
}


export function getViewPage(id:String):AxiosPromise<ViewPage> {
    return request({
      url: '/affair/viewPage/get/'+id,
      method: 'get'
    });
}

