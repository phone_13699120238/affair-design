import request from '@/utils/request';
import { AxiosPromise } from 'axios';


export interface AppSearch {
    formId: string;
    pageNum: number;
    pageSize: number;
}

export interface AppPage {
  pageNum: number;
  pageSize: number;
  total: number;
  list: App[]
}


export interface App {
    id?: string;
    name: string;
}

export function appSearch(data: AppSearch): AxiosPromise<AppPage> {
    return request({
      url: '/affair/app/search',
      data: data,
      method: 'post'
    });
}


export function addApp(data:App) {
    return request({
      url: '/affair/app/add',
      data:data,
      method: 'post'
    });
}

export function editApp(data:App) {
    return request({
      url: '/affair/app/edit',
      data:data,
      method: 'post'
    });
}


export function removeApp(data:String[]) {
    return request({
      url: '/affair/app/remove',
      data: data,
      method: 'post'
    });
}