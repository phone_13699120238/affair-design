import { RouteRecordRaw } from 'vue-router';
import { defineStore } from 'pinia';
import { constantRoutes } from '@/router';
import { listRoutes } from '@/api/system/menu';

const modules = import.meta.glob('../../views/**/**.vue');
export const Layout = () => import('@/layout/index.vue');

const hasPermission = (roles: string[], route: RouteRecordRaw) => {
  if (route.meta && route.meta.roles) {
    if (roles.includes('ROOT')) {
      return true;
    }
    return roles.some((role) => {
      if (route.meta?.roles !== undefined) {
        return (route.meta.roles as string[]).includes(role);
      }
    });
  }
  return false;
};

export const filterAsyncRoutes = (
  routes: RouteRecordRaw[],
  //roles: string[]
) => {
  const res: RouteRecordRaw[] = [];
  routes.forEach((route) => {
    const tmp = { ...route } as any;
   // if (hasPermission(roles, tmp)) {
      if (tmp.component == 'Layout') {
        tmp.component = Layout;
      } else {
        const component = modules[`../../views/${tmp.component}.vue`] as any;
        if (component) {
          tmp.component = modules[`../../views/${tmp.component}.vue`];
        } else {
          tmp.component = modules[`../../views/error-page/404.vue`];
        }
      }
      res.push(tmp);

      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children);
      }
  //  }
  });
  return res;
};


/**
 * 权限类型声明
 */
 export interface PermissionState {
  routes: RouteRecordRaw[];
  addRoutes: RouteRecordRaw[];
  isLoad: Boolean;
}

const usePermissionStore = defineStore({
  id: 'permission',
  state: (): PermissionState => ({
    routes: [],
    addRoutes: [],
    isLoad: false,
  }),
  actions: {
    setRoutes(routes: RouteRecordRaw[]) {
      this.addRoutes = routes;
      this.routes = constantRoutes.concat(routes);
    },
    generateRoutes(roles: string[]) {
      return new Promise((resolve, reject) => {
        listRoutes()
          .then((response) => {
           /* const asyncRoutes: RouteRecordRaw[]= [
              {
                "path":"/system",
                "component":"Layout",
                "redirect":"/system/user",
                "meta":{
                    "title":"系统管理",
                    "icon":"system",
                    "hidden":false,
                    "alwaysShow":true,
                    "roles":[
                        "ADMIN"
                    ],
                    "keepAlive":true
                },
                "children":[
                  {
                      "path":"user",
                      "component":"system/user/index",
                      "name":"user",
                      "meta":{
                          "title":"用户管理",
                          "icon":"user",
                          "hidden":false,
                          "alwaysShow":false,
                          "roles":[
                              "ADMIN"
                          ],
                          "keepAlive":true
                      }
                  }
                ]
              }
            ]; */
            const asyncRoutes = response.data;
            const accessedRoutes = filterAsyncRoutes(asyncRoutes);
            this.setRoutes(accessedRoutes);
            this.isLoad = true;
            resolve(accessedRoutes);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
});

export default usePermissionStore;
